{-# LANGUAGE Rank2Types #-}

type S = Monad m => [m a] -> m [a]

sequence'1 :: S
sequence'1 [] = return []
sequence'1 (m:ms) = m >>= \a ->
                              do as <- sequence'1 ms
                                 return (a : as)

-- Type error: Zero value (return ()) in fold induces (m ()), not (m [a]).
--sequence'2 :: S
--sequence'2 ms = foldr func (return ()) ms
--  where
--    func :: (Monad m) => m a -> m [a] -> m [a]
--    func m acc = do x <- m
--                    xs <- acc
--                    return (x:xs)

-- Type error: (func) induces (a -> [a] -> [a]) instead of the declared (m a -> m [a] -> m [a]).
--sequence'3 :: S
--sequence'3 ms = foldr func (return []) ms
--  where
--    func :: (Monad m) => m a -> m [a] -> m [a]
--    func m acc = m : acc

-- Syntax error: Generator desugaring not supported outside do blocks and list comprehensions.
--sequence'4 :: S
--sequence'4 [] = return ()
--sequence'4 (m:ms) = return (a:as)
--  where
--    a <- m
--    as <- sequence'4 ms

sequence'5 :: S
sequence'5 ms = foldr func (return []) ms
  where
    func :: (Monad m) => m a -> m [a] -> m [a]
    func m acc = do x <- m
                    xs <- acc
                    return (x:xs)

-- Type error: (>>) expects a nullary function.
--sequence'6 :: S
--sequence'6 (m:ms) = m >> \a ->
--                          do as <- sequence'6 ms
--                             return (a:as)

-- Syntax error: Missing 'do' block declaration.
--sequence'7 :: S
--sequence'7 [] = return []
--sequence'7 (m:ms) = m >>= \a ->
--  as <- sequence'7 ms
--  return (a:as)

sequence'8 :: S
sequence'8 [] = return []
sequence'8 (m:ms) = do a <- m
                       as <- sequence'8 ms
                       return (a:as)

test = do
  -- All console outputs should be "123456789"
  let putChar' x = putChar x >> return x
      xs = map putChar' ['1'..'9'] :: [IO Char]
      one n f = do putStr ("sequence'" ++ show n ++ ": ")
                   ys <- f xs
                   putStrLn (", " ++ ys)
  one 1 sequence'1
  one 5 sequence'5
  one 8 sequence'8

