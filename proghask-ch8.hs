import Data.Char

newtype Parser a = S (String -> [(a, String)])

failure :: Parser a
failure = S $ \inp -> []

item :: Parser Char
item = S $ \inp -> case inp of
                    [] -> []
                    (x:xs) -> [(x,xs)]

parse :: Parser a -> String -> [(a, String)]
parse (S p) inp = p inp

instance Monad Parser where
  return v = S $ \inp -> [(v, inp)]
  p >>= f = S $ \inp -> case parse p inp of
                         [] -> []
                         [(v, out)] -> parse (f v) out

(+++) :: Parser a -> Parser a -> Parser a
p +++ q = S $ \inp -> case parse p inp of
                       [] -> parse q inp
                       [(v, out)] -> [(v, out)]

sat :: (Char -> Bool) -> Parser Char
sat p = do x <- item
           if p x then return x else failure

digit = sat isDigit
lower = sat isLower
upper = sat isUpper
letter = sat isAlpha
alphanum = sat isAlphaNum
char = sat . (==)

string :: String -> Parser String
string [] = return []
string (x:xs) = do char x
                   string xs
                   return (x:xs)

many :: Parser a -> Parser [a]
many p = many1 p +++ return []

many1 :: Parser a -> Parser [a]
many1 p = do v <- p
             vs <- many p
             return (v:vs)

ident :: Parser String
ident = do x <- lower
           xs <- many alphanum
           return (x:xs)

nat :: Parser Int
nat = do xs <- many1 digit
         return (read xs)

space :: Parser ()
space = do many (sat isSpace)
           return ()

token :: Parser a -> Parser a
token p = do space
             v <- p
             space
             return v

identifier :: Parser String
identifier = token ident

natural :: Parser Int
natural = token nat

symbol :: String -> Parser String
symbol xs = token (string xs)

p :: Parser [Int]
p = do symbol "["
       n <- natural
       ns <- many (do symbol ","
                      natural)
       symbol "]"
       return (n:ns)

-- Simple calculator parse
--
-- expr ::= term (+ expr | e)
-- term ::= factor (* term | e)
-- factor ::= (expr) | nat
-- nat ::= 0|1|2|...
--
-- Expressed below, but returning running result of computation rather than a tree.
--
expr :: Parser Int
expr = do t <- term
          do symbol "+"
             e <- expr
             return (t+e)
            +++ return t

term :: Parser Int
term = do f <- factor
          do symbol "*"
             t <- term
             return (f*t)
            +++ return f

factor :: Parser Int
factor = do symbol "("
            e <- expr
            symbol ")"
            return e
           +++ natural

eval :: String -> Int
eval xs = case parse expr xs of
               [(n,[])] -> n
               [(_,out)] -> error ("unconsumed input " ++ out)
               [] -> error "invalid input"

-- Ex1
int :: Parser Int
int = do char "-"
         n <- nat
         return (-n)
        +++ nat

-- Ex2
comment :: Parser ()
comment = do string "--"
             sat (/= '\n')
             return ()

