find k t = [v | (k',v) <- t, k == k']

positions x xs = [i | (x',i) <- zip xs [0..n], x == x'] 
                 where n = length xs - 1

positions' x xs = find x (zip xs [0..length xs - 1])
