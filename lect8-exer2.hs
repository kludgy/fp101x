putStr' [] = return ()
putStr' (x:xs) = putChar x >> putStr' xs

putStrLn'1 [] = putChar '\n'
putStrLn'1 xs = putStr' xs >> putStrLn'1 ""

putStrLn'2 [] = putChar '\n'
putStrLn'2 xs = putStr' xs >> putChar '\n'

putStrLn'3 [] = putChar '\n'
putStrLn'3 xs = putStr' xs >>= \ x -> putChar '\n'

-- Type error: (>>) expects a nullary function.
--putStrLn'4 [] = putChar '\n'
--putStrLn'4 xs = putStr' xs >> \ x -> putChar '\n'

putStrLn'5 [] = putChar '\n'
putStrLn'5 xs = putStr' xs >> putStr' "\n"

-- Invalid: Prints (xs) followed by endless '\n\n'.
--putStrLn'6 [] = putChar '\n'
--putStrLn'6 xs = putStr' xs >> putStrLn'6 "\n"

-- Type error: Return must be (IO ()), not (IO String).
--putStrLn'7 [] = return ""
--putStrLn'7 xs = putStrLn'7 xs >> putStr' "\n"

-- Type error: (putChar) expects Char, not String.
--putStrLn'8 [] = putChar "\n"
--putStrLn'8 xs = putStr' xs >> putChar "\n"

