foldLeftM :: Monad m => (a -> b -> m a) -> a -> [b] -> m a
foldLeftM f a [] = return a
foldLeftM f a (x:xs) = do a' <- f a x
                          foldLeftM f a' xs

exerFunc = foldLeftM (\a b -> putChar b >> return (b : a ++ [b])) [] "haskell" >>= \r -> putStrLn r

test = do let x =  foldl     (\x y ->    x-y ) 0 [1..10]
          y     <- foldLeftM (\x y -> return (x-y)) 0 [1..10]
          print x
          print y

