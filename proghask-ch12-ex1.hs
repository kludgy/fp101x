
-- Identify redexes of the expressions below.
-- Determine whether each redex is innermost, outermost, neither or both.

-- 1 + (2 * 3)
--
-- Can't apply + first, because arithmetic is strict.
--
-- 1 + (2 * 3)
-- { apply * }
-- 1 + 6
-- { apply + }
-- 7
-- ^^^^^^^^^^^^^ innermost


-- (1 + 2) * (2 + 3)
--
-- (1 + 2) * (2 + 3)
-- { apply first + }
-- 3 * (2 + 3)
-- { apply second + }
-- 3 * 5
-- { apply * }
-- 15
-- ^^^^^^^^^^^^^ innermost
--
-- Can't apply * first, because arithmetic is strict.


-- fst (1 + 2, 2 + 3)
--
-- fst (1 + 2, 2 + 3)
-- { apply first + }
-- fst (3, 2 + 3)
-- { apply second + }
-- fst (3, 5)
-- { apply fst }
-- 3
-- ^^^^^^^^^^^^^ innermost
--
-- fst (1 + 2, 2 + 3)
-- { apply fst }
-- 1 + 2
-- { apply + }
-- 3
-- ^^^^^^^^^^^^^ outermost


-- (\x -> 1 + x) (2 * 3)
--
-- (\x -> 1 + x) (2 * 3)
-- { apply * }
-- (\x -> 1 + x) 6
-- { apply \ }
-- 1 + 6
-- { apply + }
-- 7
-- ^^^^^^^^^^^^^ both: innermost because \ is strict in param eval, technically outermost in the expression of the expanded \.

