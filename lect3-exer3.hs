
True `con1` True = True
_    `con1` _    = False

a `con2` b = if a then if b then True else False else False

a `con3` b = if not a then not b else True

-- Bogus syntax in con4 due to lack of else in conditional expression.
-- a `con4` b = if a then b

a `con5` b = if a then if b then False else True else False

a `con6` b = if a then b else False

a `con7` b = if b then a else False

tests = [(False,False),(False,True),(True,True),(True,False)]
con_results f = map (\(a,b) -> a `f` b) tests
con_proof = con_results (&&)

doTests = map (\(s,f) -> (s, con_results f == con_proof))  [("1",con1),("2",con2),("3",con3),("5",con5),("6",con6),("7",con7)]
