-- 6. Define appropriate versions of the library functions
--
-- repeat :: a → [a ]
-- repeat x = xs where xs = x : xs
-- take :: Int → [a ] → [a ]
-- take 0 = []
-- take (n + 1) [ ] = [ ]
-- take (n + 1) (x : xs) = x : take n xs
-- replicate :: Int → a → [a ]
-- replicate n = take n ◦ repeat
-- for the following type of binary trees:
--
-- data Tree a = Leaf | Node (Tree a) a (Tree a)

data Tree a = Leaf | Node (Tree a) a (Tree a)

btrepeat :: a -> Tree a
btrepeat x = Node (btrepeat x) x (btrepeat x)

bttake :: Int -> Tree a -> Tree a
bttake 0 _    = Leaf
bttake _ Leaf = Leaf
bttake n (Node l x Leaf) =
bttake n (Node l x r) = Node (bttake (n-1) l) x (bttake (n-1-m))

flatten :: Tree a -> [a]
flatten Leaf = []
flatten (Node l x r) = flatten l ++ x : flatten r

unflatten :: [a] -> Tree a
unflatten [] = Leaf
unflatten [x] = Node Leaf x Leaf
unflatten (x:y:xs) = ??

-- Not sure how to do this yet.
--
