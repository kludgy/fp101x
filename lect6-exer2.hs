
-- Only interested in booleans for testing propositions, since predicates
-- reduce to this anyway.
p :: Bool -> Bool
p = id

-- Test data
xss = [[False,False],[False,True],[True,False],[True,True]]

test :: ([Bool] -> Bool) -> ([Bool] -> Bool) -> IO ()
test f g = do
  let us = map f xss
      vs = map g xss
  print $ (show us) ++ ", " ++ (show vs) ++ " --> " ++ if us == vs then "OK" else "FAIL"

inadmissable = print "Inadmissable."

alltests = do
  inadmissable --test (       any p   ) (       map p . or                             )
  test (       any p   ) (       or . map p                             )
  test (\xs -> any p xs) (\xs -> length (filter p xs) > 0               )
  test (       any p   ) (       not . null . dropWhile (not . p)       )
  test (       any p   ) (       null . filter p                        )
  test (\xs -> any p xs) (\xs -> not (all (\x -> not (p x)) xs)         )
  test (\xs -> any p xs) (\xs -> foldr (\x acc -> (p x) || acc) False xs)
  test (\xs -> any p xs) (\xs -> foldr (||) True (map p xs)             )
