ys = [4,5,6]
n = length ys

f1 = concat [ zip (replicate n 1) ys, zip (replicate n 2) ys, zip (replicate n 3) ys ]
f2 = concat [ zip (replicate n x) ys | x <- [1,2,3] ]


f3 = do x <- [1,2,3]
        y <- [4,5,6]
        return (x,y)

f4 = [1,2,3] >>= (\x -> [4,5,6] >>= (\y -> return (x,y)))

f5 = bind [1,2,3] (\x -> bind [4,5,6] (\y -> [(x,y)]))
     where bind xs f = concat [f x | x <- xs]

f6 = concat [ (\x -> ([ (\y -> (x,y)) y' | y' <- [4,5,6] ])) x' | x' <- [1,2,3] ]
f7 = concat [[(x,y) | y <- [4,5,6]] | x <- [1,2,3]]

