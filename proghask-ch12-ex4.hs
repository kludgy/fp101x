-- 4. Using a list comprehension, define an expression fibs :: [Integer] that
-- generates the infinite sequence of Fibonacci numbers
-- 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, · · ·
-- using the following simple procedure:
-- • The first two numbers are 0 and 1;
-- • The next is the sum of the previous two;
-- • Return to the second step.
-- Hint: make use of the library functions zip and tail . Note that numbers
-- in the Fibonacci sequence quickly become large, hence the use of the
-- type Integer of arbitrary-precision integers above.

import Data.List

-- fibs = 0 : 1 : zipWith (+) fibs (tail fibs)
-- { applying outer fibs }
-- 0 : 1 : zipWith (+) fibs (tail fibs)
-- { applying zipWith }
-- 0 : 1 : ((+) (head fibs) (head $ tail fibs) : zipWith (+) (tail fibs) (tail $ tail fibs))
-- { applying (head fibs) and (head $ tail fibs) }
-- 0 : 1 : ((+) 0 1 : zipWith (+) (tail fibs) (tail $ tail fibs))
-- { ... }

fibs :: [Integer]
fibs = 0 : 1 : zipWith (+) fibs (tail fibs)

