
-- Tree is intended to be a search tree
data Tree = Leaf Integer
          | Node Tree Integer Tree

type O = Integer -> Tree -> Bool

occurs1 :: O
occurs1 m (Leaf n) = m == n
occurs1 m (Node l n r)
  = case compare m n of
         LT -> occurs1 m l
         EQ -> True
         GT -> occurs1 m r

-- occurs2 is out because LT/RT descend into the wrong sides of the search tree: ((nodes l) < n < (nodes r))
--occurs2 m (Leaf n) = m == n
--occurs2 m (Node l n r)
--  = case compare m n of
--         LT -> occurs2 m r
--         EQ -> True
--         GT -> occurs2 m l

-- occurs3 is out because of a type error in the Leaf case. (Returns Ordering instead of Bool.)

-- occurs4 is out because the EQ case returns False instead of True.

-- occurs5: OK

-- occurs6 is out because cases descend into the wrong sides of the search tree. (Same problem as occurs2.)

-- occurs7 and occurs8 are out because of type errors, matching (n) instead of (Leaf n).

