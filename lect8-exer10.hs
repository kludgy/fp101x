foldRightM :: Monad m => (a -> b -> m b) -> b -> [a] -> m b
foldRightM f b [] = return b
foldRightM f b (x:xs) = do ys <- foldRightM f b xs
                           f x ys

exerFunc = foldRightM (\a b -> putChar a >> return (a : b)) [] (show [1,3..10]) >>= \r -> putStrLn r

exerFunc' = do r <- foldRightM (\a b -> putChar a >> return (a:b)) [] (show [1,3..10])
               putStrLn r

test = do let x =  foldr      (\x y ->    x-y ) 0 [1..10]
          y     <- foldRightM (\x y -> return (x-y)) 0 [1..10]
          print x
          print y

