{-# LANGUAGE Rank2Types #-}

type S = Monad m => (a -> m Bool) -> [a] -> m [a]

-- Doesn't use the predicate.
--filterM'1 :: S
--filterM'1 _ [] = return []
--filterM'1 p (x:xs)
--  = do flag <- p x
--       ys <- filterM'1 p xs
--       return (x:ys)

filterM'2 :: S
filterM'2 _ [] = return []
filterM'2 p (x:xs)
  = do flag <- p x
       ys <- filterM'2 p xs
       if flag then return (x:ys) else return ys

-- Type error: Conditional requires (Bool), not (m Bool).
--filterM'3 :: S
--filterM'3 _ [] = return []
--filterM'3 p (x:xs)
--  = do ys <- filterM'3 p xs
--       if p x then return (x:ys) else return ys

-- Incorrect: Predicate is negated.
--filterM'4 :: S
--filterM'4 _ [] = return []
--filterM'4 p (x:xs)
--  = do flag <- p x
--       ys <- filterM'4 p xs
--       if flag then return ys else retyrn (x:ys)

