-- Just notes for now. Does not compile.


import Prelude hiding (foldl, foldr)

foldr            :: (a -> b -> b) -> b -> [a] -> b
foldr _ z []      =  z
foldr f z (x:xs)  =  x `f` (foldr f z xs)

foldl            :: (b -> a -> b) -> b -> [a] -> b
foldl _ z []      =  z
foldl f z (x:xs)  =  foldl f (z `f` x) xs

  foldr f z (x1:xs1)
= x1 `f` foldr f z xs1
= x1 `f` (x2 `f` foldr f z xs2)
= x1 `f` (x2 `f` (x3 `f` foldr f z xs3))
..

  foldl f z (x1:xs1)
= foldl f (z `f` x1) xs1
= foldl f (z `f` x1 `f` x2) xs2
= foldl f (z `f` x1 `f` x2 `f` x3) xs3
..


--foldl :: (b -> a -> b) -> b -> [a] -> b
--foldr :: (a -> b -> b) -> b -> [a] -> b
--flip  :: (a -> b -> c) -> (b -> a -> c)
--id    :: (a -> a)
--(.)   :: (b -> c) -> (a -> b) -> (a -> c)
--($)   :: (a -> b) -> (a -> b)


foldl'1 f a bs = foldr (\b -> \g -> (\a -> g (f a b))) id bs a

  foldl'1 f a bs
= foldl f a bs
= foldr (\b -> \g -> (\a -> g (f a b))) id bs a
= foldr (\b g a -> g (f a b)) id bs a

b -> (b -> c) -> a -> c
a -> a
[b]
a

=

foldl'2 f a bs = foldr (\a b -> f b a) a bs

foldl'3 f = flip $ foldr (\a b g -> b (f g a)) id

foldl'4 = foldr . flip


apply f = f (-) 123 [1..10000] :: Integer

test i f = do
  let ok = apply f == apply foldl
  putStrLn ("foldl'" ++ show i ++ ": " ++ show ok)

testall = do
  test 1 foldl'1
  test 2 foldl'2
  test 3 foldl'3
  test 4 foldl'4



