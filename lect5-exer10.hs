
merge [] ys = ys
merge xs [] = xs
merge (x:xs) (y:ys)
  = if x <= y then x : merge xs (y:ys) else y : merge (x:xs) ys

halve :: [a] -> ([a], [a])
halve xs = splitAt (length xs `div` 2) xs

-- msort' is missing the singleton [x] case that allows recursion to terminate.
-- (ie. halve [x] can divide the list no further, so results will always match the singleton case!)
msort' :: Ord a => [a] -> [a]
msort' [] = []
msort' xs = merge (msort' ys) (msort' zs)
  where (ys, zs) = halve xs

msort :: Ord a => [a] -> [a]
msort [] = []
msort [x] = [x]
msort xs = merge (msort ys) (msort zs)
  where (ys, zs) = halve xs
