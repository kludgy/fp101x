True  `dis1` True  = True
True  `dis1` False = True
False `dis1` False = False
False `dis1` True  = True

True  `dis2` _     = True
_     `dis2` True  = True
False `dis2` False = False

True  `dis3` _     = True
_     `dis3` True  = True
_     `dis3` _     = False

False `dis4` False = False
_     `dis4` _     = True
