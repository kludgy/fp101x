{-# LANGUAGE Rank2Types #-}

sequence' :: Monad m => [m a] -> m [a]
sequence' [] = return []
sequence' (m:ms) = do a <- m
                      as <- sequence' ms
                      return (a:as)

sequence_' :: Monad m => [m a] -> m ()
sequence_' ms = sequence ms >> return ()

type S = Monad m => (a -> m b) -> [a] -> m [b]

mapM'1 :: S
mapM'1 f as = sequence' (map f as)

mapM'2 :: S
mapM'2 f [] = return []
mapM'2 f (a:as) = f a >>= \b -> mapM'2 f as >>= \bs -> return (b:bs)

-- Type error: (sequence_') returns (m ()), not (m [a]).
--mapM'3 :: S
--mapM'3 f as = sequence_' (map f as)

-- Type error: (>>) expects a unary function.
--mapM'4 :: S
--mapM'4 f [] = return []
--mapM'4 f (a:as) = f a >> \b -> mapM'4 f as >> \bs -> return (b:bs)

-- Syntax error: Lambda arrow (->) doesn't make sense in a do block. Probably meant draw-from (<-).
--mapM'5 :: S
--mapM'5 f [] = return []
--mapM'5 f (a:as) =
--  do
--    f a -> b
--    mapM' f as -> bs
--    return (b:bs)

mapM'6 :: S
mapM'6 f [] = return []
mapM'6 f (a:as) = do b <- f a
                     bs <- mapM'6 f as
                     return (b:bs)

mapM'7 :: S
mapM'7 f [] = return []
mapM'7 f (a:as)
  = f a >>=
      \b ->
        do bs <- mapM'7 f as
           return (b:bs)

-- Incorrect: This will end up rotating the entire list left by one element.
--mapM'8 :: S
--mapM'8 f [] = return []
--mapM'8 f (a:as)
--  = f a >>=
--      \b ->
--        do bs <- mapM f as
--           return (bs ++ [b])

test = do
  let one n f = do putStr ("mapM'" ++ show n ++ ": ")
                   ys <- f (return . (+1)) [1..9]
                   putStrLn (show ys ++ ", " ++ if ys == [2..10] then "OK" else "Failed")
  one 1 mapM'1
  one 2 mapM'2
  one 6 mapM'6
  one 7 mapM'7

