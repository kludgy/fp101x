module ProgHaskCh7Ex8 (transmit) where

import Data.Char

type Bit = Int
type Parity = Int

transmit :: String -> String
transmit = decode . channel . encode

channel :: [Bit] -> [Bit]
channel = id

unsafeParseParity :: [Bit] -> [Bit]
unsafeParseParity (x:xs)
    | parity xs /= x = error "bad parity"
    | otherwise = xs

decode :: [Bit] -> String
decode = map (chr . bin2int) . map unsafeParseParity . chop 9

encode :: String -> [Bit]
encode = concat . map ((\xs -> parity xs : xs) . make8 . int2bin . ord)

chop8 :: [Bit] -> [[Bit]]
chop8 = chop 8

chop :: Int -> [Bit] -> [[Bit]]
chop _ [] = []
chop n xs = take n xs : chop n (drop n xs)

make8 :: [Bit] -> [Bit]
make8 xs = take 8 (xs ++ repeat 0)

int2bin :: Int -> [Bit]
int2bin 0 = []
int2bin n = n `mod` 2 : int2bin (n `div` 2)

bin2int :: [Bit] -> Int
bin2int = foldr (\x y -> x + 2 * y) 0

parity :: [Bit] -> Bit
parity = (`mod` 2) . length . filter (/= 0)
