
False `dis1` False = False
_     `dis1` _     = True

False `dis2` b     = b
True  `dis2` _     = True

b `dis3` c | b == c    = True
           | otherwise = False

b `dis4` c | b == c    = b
           | otherwise = True

b `dis5` False    = b
_ `dis5` True     = True

b `dis6` c | b == c    = c
           | otherwise = True

b `dis7` True = b
_ `dis7` True = True


-- Skipping truth table. Easy to visually verify.


tests = [(False,False),(False,True),(True,True),(True,False)]
dis_results f = map (\(a,b) -> a `f` b) tests
dis_proof = dis_results (||)

doTests = map (\f -> dis_results f == dis_proof)  [dis1,dis2,dis3,dis4,dis5,dis6,dis7]
