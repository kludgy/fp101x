x `con` y = if x == True 
              then y 
              else if x == False 
                     then False 
                     else undefined  -- Need to provide tailing else to satisfy syntax, but it cannot be reached.
