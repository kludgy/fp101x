safetail_cond xs = if null xs then [] else tail xs

safetail_guard xs | null xs = []
                  | otherwise = tail xs

safetail_pattern [] = []
safetail_pattern xs = tail xs
