
-- Only interested in booleans for testing propositions, since predicates
-- reduce to this anyway.
p :: Bool -> Bool
p = id

-- Test data
xss = [[False,False],[False,True],[True,False],[True,True]]

test :: ([Bool] -> Bool) -> ([Bool] -> Bool) -> IO ()
test f g = do
  let us = map f xss
      vs = map g xss
  print $ (show us) ++ ", " ++ (show vs) ++ " --> " ++ if us == vs then "OK" else "FAIL"

inadmissable = print "Inadmissable."

alltests = do
  test (\xs -> all p xs) (\xs -> and (map p xs)             )
  inadmissable --  test (\xs -> all p xs) (\xs -> map p (and xs)             )
  test (       all p   ) (       and . map p                )
  test (       all p   ) (       not . any (not . p)        )
  inadmissable --  test (       all p   ) (       map p . and                )
  test (\xs -> all p xs) (\xs -> foldl (&&) True  (map p xs))
  test (\xs -> all p xs) (\xs -> foldr (&&) False (map p xs))
  test (       all p   ) (       foldr (&&) True . map p    )
