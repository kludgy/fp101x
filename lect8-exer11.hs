{-# LANGUAGE Rank2Types #-}

type S = Monad m => (a -> b) -> m a -> m b

liftM'1 :: S
liftM'1 f m
  = do x <- m
       return (f x)

-- Type error: Result must be (m b), not (b).
--liftM'2 :: S
--liftM'2 f m = m >>= \ a -> f a

-- This is just a desugaring of the syntax in liftM'1: Replace (a) with (x) and rewrite
-- (>>=) application with equivalent do notation draw-from (<-).
liftM'3 :: S
liftM'3 f m = m >>= \a -> return (f a)

-- Type error: (f) applies to (a), not (m a).
--liftM'4 :: S
--liftM'4 f m = return (f m)

-- Incorrect: (m a) must be applied once, not twice.
--liftM'5 :: S
--liftM'5 f m = m >>= \ a -> m >>= \ b -> return (f a)

-- Incorrect: (m a) must be applied once, not twice.
--liftM'6 :: S
--liftM'6 f m = m >>= \ a -> m >>= \ b -> return (f b)

-- Type error: (mapM) applies to (a -> m b), not (a -> b).
--liftM'7 :: S
--liftM'7 f m = mapM f [m]

-- Type error: (>>) requires a nullary function.
--liftM'8 :: S
--liftM'8 f m = m >> \ a -> return (f a)

