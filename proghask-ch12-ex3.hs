
-- mult = \x -> (\y -> x*y)
--
-- mult 3 4
-- { apply mult }
-- (\x -> (\y -> x*y)) 3 4
-- { apply outer \ }
-- (\y -> 3*y) 4
-- { apply \ }
-- 3*4
-- { apply * }
-- 12

