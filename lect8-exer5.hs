{-# LANGUAGE Rank2Types #-}

type S = Monad m => [m a] -> m ()

-- Type error: (>>) expects a nullary function parameter.
--sequence_'1 :: S
--sequence_'1 [] = return []
--sequence_'1 (m:ms) = m >> \_ -> sequence_'1 ms

sequence_'2 :: S
sequence_'2 [] = return ()
sequence_'2 (m:ms) = (foldl (>>) m ms) >> return ()

-- Type error: (return ()) as a zero value in the fold induces [m ()] instead of [m a].
--sequence_'3 :: S
--sequence_'3 ms = foldl (>>) (return ()) ms

sequence_'4 :: S
sequence_'4 [] = return ()
sequence_'4 (m:ms) = m >> sequence_'4 ms

sequence_'5 :: S
sequence_'5 [] = return ()
sequence_'5 (m:ms) = m >>= \_ -> sequence_'5 ms

-- Type error: (>>=) requires a unary function, but the elements in ms are nullary.
--sequence_'6 :: S
--sequence_'6 ms = foldr (>>=) (return ()) ms

sequence_'7 :: S
sequence_'7 ms = foldr (>>) (return ()) ms

-- Type error: (return []) as a zero value in the fold induces a resulting type of (forall b. m [b]) instead of (m ()).
--sequence_'8 :: S
--sequence_'8 ms = foldr (>>) (return []) ms

test = do
  -- All console outputs should be "123456789"
  let xs = map putChar ['1'..'9'] :: [IO ()]
      one n f = do putStr ("sequence_'" ++ show n ++ ": ")
                   f xs
                   putStrLn ""
  one 2 sequence_'2
  one 4 sequence_'4
  one 5 sequence_'5
  one 7 sequence_'7

